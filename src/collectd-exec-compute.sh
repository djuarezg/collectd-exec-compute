#!/bin/bash

HOSTNAME="${COLLECTD_HOSTNAME:-$(hostname -f)}"
INTERVAL="${COLLECTD_INTERVAL:-60}"
EXEC_ONCE="${EXEC_ONCE:-0}"

function put_value() {
  local type="$1"
  local data="$2"
  echo "PUTVAL \"${HOSTNAME}/exec_compute/gauge-${type}\" interval=${INTERVAL} N:${data}"
}

while : ; do
  # Send values for cpu metrics
  cpudata=$(/usr/bin/lscpu)
  ret=$?
  # If the execution of lscpu fails, we cannot ensure that the data is correct so we will skip it
  if [ $ret -eq 0 ]; then
    put_value 'cpus'     "$(echo "$cpudata" | grep -Ew '^CPU\(s\)'                | cut -d':' -f2 | sed 's/ //g')"
    put_value 'sockets'  "$(echo "$cpudata" | grep -Ew '^Socket\(s\)'             | cut -d':' -f2 | sed 's/ //g')"
    put_value 'cores'    "$(echo "$cpudata" | grep -Ew '^Core\(s\)\ per\ socket'  | cut -d':' -f2 | sed 's/ //g')"
    put_value 'threads'  "$(echo "$cpudata" | grep -Ew '^Thread\(s\)\ per\ core'  | cut -d':' -f2 | sed 's/ //g')"

    # Send notifications for hugepages metrics
    file2M="/sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages"
    put_value 'huge2048k' "$([ -f $file2M ] && cat $file2M || echo "0")"
  fi

  # Calculate virt_status for host validation
  validatedata=$(virt-host-validate qemu)

  virt_enabled=$(echo "$validatedata" | grep -Ew '^\s\sQEMU:\sChecking\sfor\shardware\svirtualization'        | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)
  kvm_exist=$(echo "$validatedata" | grep -Ew '^\s\sQEMU:\sChecking\sif\sdevice\s/dev/kvm\sexists'         | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)
  kvm_accessible=$(echo "$validatedata" | grep -Ew '^\s\sQEMU:\sChecking\sif\sdevice\s/dev/kvm\sis\saccessible' | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)
  vhost_net_exists=$(echo "$validatedata" | grep -Ew '^\s\sQEMU:\sChecking\sif\sdevice\s/dev/vhost-net\sexists'   | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)
  net_tun_exists=$(echo "$validatedata" | grep -Ew '^\s\sQEMU:\sChecking\sif\sdevice\s/dev/net/tun\sexists'     | cut -d':' -f3 | sed 's/ //g' | grep -Ewoq 'PASS' && echo 1 || echo 0)

  # Send just one value
  put_value 'virt_status' "$([ "$virt_enabled" -eq 1 ] && [ "$kvm_exist" -eq 1 ] && [ "$kvm_accessible" -eq 1 ] && [ "$vhost_net_exists" -eq 1 ] && [ "$vhost_net_exists" -eq 1 ] && [ "$net_tun_exists" -eq 1 ] && echo 1 || echo 0)"

  put_value 'disks' "$(/usr/bin/lsscsi | wc -l)"

  put_value 'memory' "$(/usr/bin/journalctl -b -t kernel | grep -E 'Memory:' | cut -d '/' -f2 | cut -d 'k' -f1)"

  if [ "$EXEC_ONCE" -eq 1 ]; then
    break
  fi
  sleep "$INTERVAL"
done

exit 0
