
%global libvirt_version  3.9.0

Name:           collectd-exec-compute
Version:        1.1.0
Release:        2%{?dist}
Summary:        Simple collectd exec plugin to retrieve metrics from a compute node
License:        GPLv3
Group:          Development/Libraries
BuildArch:      noarch
Url:            https://gitlab.cern.ch/cloud-infrastructure/collectd-exec-compute

Source0:        %{name}-%{version}.tar.gz

Requires:       collectd
Requires:       libvirt-client >= %{libvirt_version}
Requires:       util-linux

Requires(post):   policycoreutils, collectd
Requires(preun):  policycoreutils, collectd
Requires(postun): policycoreutils

BuildRequires:  selinux-policy
BuildRequires:  selinux-policy-devel
BuildRequires:  selinux-policy-targeted
BuildRequires:  checkpolicy

%{?systemd_requires}
BuildRequires: systemd

%description
Simple collectd exec plugin that retrieves metrics from a compute node if 
like cpus, sockets, cores, threads and 2M hugepages. It also fecthes the
status of the hypervisor if it is able to host QEMU virtual machines through
virt-host-validate.

%prep
%setup -q -n %{name}-%{version}

%build
%install
install -p -D -m 755 src/collectd-exec-compute.sh %{buildroot}%{_bindir}/collectd-exec-compute.sh

# selinux
make -f %{_datadir}/selinux/devel/Makefile collectd_exec_compute.pp src/collectd_exec_compute.te
install -p -m 644 -D collectd_exec_compute.pp %{buildroot}%{_datadir}/selinux/packages/%{name}/collectd_exec_compute.pp

%post
if [ "$1" -le "1" ] ; then # First install
semodule -i %{_datadir}/selinux/packages/%{name}/collectd_exec_compute.pp 2>/dev/null || :
fi
%systemd_post collectd.service

%preun
if [ "$1" -lt "1" ] ; then # Final removal
semodule -r collectd_exec_compute 2>/dev/null || :
fi
%systemd_preun collectd.service

%postun
if [ "$1" -ge "1" ] ; then # Upgrade
semodule -i %{_datadir}/selinux/packages/%{name}/collectd_exec_compute.pp 2>/dev/null || :
fi
%systemd_postun_with_restart collectd.service

%files
%attr(0755, root, root) %{_bindir}/collectd-exec-compute.sh
%attr(0644, root, root) %{_datadir}/selinux/packages/%{name}/collectd_exec_compute.pp
%doc src/README.md

%changelog
* Thu Apr 16 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.0-2.el7.cern
- Added metrics for disk and memory installed

* Thu Mar 05 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.1.0-1.el7.cern
- Change notification behavior and send values directly
- Include values for detection of KVM status
- Added selinux policy for accessing /dev/kvm

* Thu Jul 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-8.el7.cern
- Ignore lscpu execution if the execution has not complete

* Thu Jul 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-7.el7.cern
- Add message in the OKAY notification

* Thu Jul 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-6.el7.cern
- Send host in the failure/okay notification as it's required now
- Put back FAILURE notification instead of ERROR

* Mon May 06 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-5.el7.cern
- Raise ERROR notification instead of FAILURE

* Tue Aug 28 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-4.el7.cern
- Fix bug on interval

* Tue Aug 28 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-3.el7.cern
- Add argument value (-1) to disable specific checks

* Fri Aug 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-2.el7.cern
- Add unknown status to send OK notifications add start
- Add restart of collectd service on upgrade

* Fri Aug 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-1.el7.cern
- Initial release of the exec plugin
